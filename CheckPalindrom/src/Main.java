
public class Main {

	public static void main(String[] args) 
	{
		//System.out.println(isPalindrom("otto"));
		//System.out.println(isPalindrom("anna"));
		//System.out.println(isPalindrom("muitin"));
		//System.out.println(isPalindrom("retter"));
		//System.out.println(isPalindrom("Hallo"));
		//System.out.println(isPalindrom("Lagerregal"));

		for (int i = 0; i<args.length; i++)
		{
			if (isPalindrom(args[i]))
			{
				System.out.println(args[i] + " is a Palindrom");
			}
			else
			{
				System.out.println(args[i] + " is not a Palindrom");

			}
		}
	}


	public static boolean isPalindrom (String pal)
	{
		pal = pal.toLowerCase();
		if(pal.length()==0 || pal.length() == 1)
		{
			//return "The word is a Palindrom!";
			return true;
		}

		if(pal.substring(0, 1).equals(pal.substring(pal.length()-1,pal. length())))
		{
			pal = pal.substring(0,1);
			pal = pal.substring(pal.length()-1,pal.length());
			return isPalindrom(pal);
		}
		//return "The word is not a Palindorm";
		return false;
	}

}