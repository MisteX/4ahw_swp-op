import java.io.*;
import java.util.ArrayList;

public class Main {

	static String extension = ".md";
	static String filename = null;
	static FileReader file=null;
	static String pathText = null;
	static String wholeText;
	static String fileSeparator;
	static String writtenText;
	static String ending = "";
	static ArrayList<String> htmlLining = new ArrayList<String>();
	static FileWriter fw;

	public static void FileInput(String FileDirectory)
	{
		try 
		{
			file = new FileReader(FileDirectory);
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		}

	}

	public static String mainHeaderchange (String changeText)
	{
		changeText = "<h1>";
		ending = "</h1>";
		return changeText;
	}

	public static String secHeaderchange (String changeText)
	{
		changeText = "<h2>";
		ending = "</h2>";
		return changeText;
	}
	public static String thirHeaderchange (String changeText)
	{
		changeText = "<h3>";
		ending = "</h3>";
		return changeText;
	}
	public static String fourHeaderchange (String changeText)
	{
		changeText = "<h4>";
		ending = "</h4>";
		return changeText;
	}
	public static String fiveHeaderchange (String changeText)
	{
		changeText = "<h5>";
		ending = "</h5>";
		return changeText;
	}
	public static String sixthHeaderchange (String changeText)
	{
		changeText = "<h6>";
		ending = "</h6>";
		return changeText;
	}
	public static String unorderdListChange (String changeText)
	{
		changeText = "<ul><li>";
		ending = "</li></ul>";		
		return changeText;
	}

	public static String breakpointchange (String changeText)
	{
		changeText = "<br/>";
		ending = null;
		return changeText;
	}
	public static String spaceCorrect (String text)
	{
		while(text.indexOf(" ")== 0)
		{
			text = text.substring(1);
		}
		return text;
	}

	public static void writeInHTMLFile()
	{
		String list1;
		String list2;
		try 
		{
			for (int i = 1; i < htmlLining.size(); i++)
			{				
				if(htmlLining.get(i - 1).contains("<ul>") && htmlLining.get(i).contains("<ul>"))
				{
					list1 = htmlLining.get(i - 1);
					list2 = htmlLining.get(i);
					
					list1 = list1.replace("</ul>", "");
					list2 = list2.replace("<ul>", "");

					htmlLining.set(i - 1, list1);
					htmlLining.set(i, list2);
				}
			}
			fw = new FileWriter(pathText + fileSeparator + filename + ".html");
			
			for (int i=0; i<htmlLining.size();i++)
			{
				writeToFile(htmlLining.get(i));
			}
			
			fw.close();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}

	public static void writeToFile(String str) throws IOException 
	{
		fw.write(str);
		fw.flush();
	}

	public static void main(String[] args) 
	{
		int lineCounter = 0;
		String command = "";
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);

		fileSeparator = System.getProperties().getProperty("file.separator");
		
		try 
		{

			int fntIndex;
			BufferedReader filelineText;

			System.out.print("Specify the file-path without filename: ");
			pathText = br.readLine();
			System.out.print("Type in the name of the file without extension: ");
			filename = br.readLine();

			FileInput(pathText + fileSeparator + filename + extension);
			filelineText = new BufferedReader(file);

			while ((wholeText = filelineText.readLine()) != null) 
			{
				wholeText = spaceCorrect(wholeText);

				if (wholeText.startsWith("#"))
				{
					fntIndex = wholeText.indexOf(" ");
					writtenText = wholeText.substring(fntIndex + 1);
					command = wholeText.substring(0, fntIndex);
				}
				else if((wholeText.startsWith("==")) || (wholeText.startsWith("--")))
				{
					writtenText = wholeText;
				}
				else if(wholeText.startsWith("*") || wholeText.startsWith("-"))
				{
					fntIndex = wholeText.indexOf(" ");
					writtenText = wholeText.substring(fntIndex + 1);
					command = wholeText.substring(0,fntIndex);
				}
				else
				{
					writtenText=wholeText;
					command = "<a>";
					ending = "</a>";

				}

				switch(command)
				{
				case "#":command = mainHeaderchange(command);break;
				case "##":command = secHeaderchange(command);break;
				case "###":command = thirHeaderchange(command);break;
				case "####":command = fourHeaderchange(command);break;
				case "#####":command = fiveHeaderchange(command);break;
				case "######":command = sixthHeaderchange(command);break;
				case "  ":command = breakpointchange(command);break;
				case "*":command = unorderdListChange(command);break;
				case "-":command = unorderdListChange(command);break;
				}

				while(writtenText.contains("***"))
				{
					writtenText = writtenText.replaceFirst("[*][*][*]","<b><i>");
					writtenText = writtenText.replaceFirst("[*][*][*]","</b></i>");
				}
				while(writtenText.contains("**"))
				{
					writtenText = writtenText.replaceFirst("[*][*]","<b>");
					writtenText = writtenText.replaceFirst("[*][*]","</b>");
				}
				while(writtenText.contains("*"))
				{
					writtenText = writtenText.replaceFirst("[*]","<i>");
					writtenText = writtenText.replaceFirst("[*]","</i>");
				}
				while(writtenText.contains("__"))
				{
					writtenText = writtenText.replaceFirst("__","<u>");
					writtenText = writtenText.replaceFirst("__","</u>");
				}

				if (writtenText.startsWith("==")) 
				{
					wholeText = htmlLining.get(lineCounter - 1);
					htmlLining.remove(lineCounter - 1);

					while(wholeText.contains("<a") || wholeText.contains("</a"))
					{
						wholeText = wholeText.replaceFirst("<a","<h1");
						wholeText = wholeText.replaceFirst("</a", "</h1");
						wholeText = wholeText.replace("<br/>","");
					}

				}
				else if(writtenText.startsWith("--"))
				{
					if(!writtenText.startsWith("---"))
					{
						wholeText =htmlLining.get(lineCounter - 1);
						htmlLining.remove(lineCounter - 1);
						
						while(wholeText.contains("<a") || wholeText.contains("</a"))
						{
							wholeText = wholeText.replaceFirst("<a","<h2");
							wholeText = wholeText.replaceFirst("</a", "</h2");
							wholeText = wholeText.replace("<br/>","");
						}
					}
					else
					{
						wholeText = "<hr>";
					}

				}
				else
				{
					wholeText = command + writtenText + ending;

					lineCounter++;
				}
				if (wholeText.contains("<h"))
				{
					wholeText = wholeText.replaceAll("<br/>", "");
				}
				wholeText = wholeText + " <br/>";
				htmlLining.add(wholeText);

				command = "";
				ending = "";
			}
		}
		catch (IOException e) 
		{
			System.err.println("Bad File!");
		}

		writeInHTMLFile();
		System.out.print("Finished converting");
	}

}