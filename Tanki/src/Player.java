public class Player 
{
	private int playerNumber;
	private int positionX;
	private int positionY;
	private int health = 100;
	private int velocity = 0;
	private int angle = 0;
	
	Randomizer r = new Randomizer();
	
	public Player (int playerNumber)
	{
		int posX = 1;
		if(playerNumber == 1 )
		{
			posX = r.getIntFromZero(ThanksView.getDimensionX() / 3);
		}
		else
		{
			posX = r.getInt(ThanksView.getDimensionX() * 2 / 3, ThanksView.getDimensionX());
		}
		setPositionX(posX);
		setPlayerNumber(playerNumber);
	}

	public int getPositionX() 
	{
		return positionX;
	}

	public void setPositionX(int positionX) 
	{
		this.positionX = positionX;
	}
	
	public int getPlayerNumber() 
	{
		return playerNumber;
	}

	public void setPlayerNumber(int playerNumber) 
	{
		this.playerNumber = playerNumber;
	}

	public int getHealth() 
	{
		return health;
	}

	public void setHealth(int health) 
	{
		this.health = health;
	}

	public int getVelocity() {
		return velocity;
	}

	public void setVelocity(int velocity) 
	{
		this.velocity = velocity;
	}

	public int getAngle() 
	{
		return angle;
	}

	public void setAngle(int angle) 
	{
		this.angle = angle;
	}

	public int getPositionY() 
	{
		return positionY;
	}

	public void setPositionY(int positionY) 
	{
		this.positionY = positionY;
	}
	
	
}
