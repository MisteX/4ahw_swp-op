public class ThanksContr 
{
	private static ThanksContr tc = null;
	private ThanksModel tm;
	private Randomizer rdm = new Randomizer();
	private Player p1;
	private Player p2;
	
	private ThanksContr () 
	{
		tm = ThanksModel.getInstance();
	}
	
	public static  ThanksContr getInstance()
	{
		if(tc == null)
		{
			tc = new ThanksContr();
		}
		return tc;
	}
	
	public void fillField(int start)
	{
		int l = tm.getField().length;
		int[] he = new int[l];
		if(start <= 90)
		{
			start = 90;
		}
		
		for(int i = 0;  i < l; i++)
		{
			if(i == 0)
			{
				he[i] = rdm.getInt(80, start);
			}
			else
			{
				while(he[i] < 80)
				{
					he[i] = rdm.getInt(he[i-1] - 2, he[i-1] + 2);
				}
				
			}
//			System.out.println(he[i]);
			
		}
		
		tm.fillField(he);
	}
	
	public void createPlayers()
	{
		p1 = new Player(1);
		p2 = new Player(2);		
	}
	
	public int[] getPlayerPositions()
	{
		int[] positions = new int[2];
		
		positions[0] = p1.getPositionX();
		positions[1] = p2.getPositionX();
		
		return positions;
	}
	
	public void changeAngle(int playerNumber, int value)
	{
		if(playerNumber == 1)
		{
			p1.setAngle(p1.getAngle() + value);
			
			System.out.println("Angle: " + p1.getAngle());
		}
		else
		{
			p2.setAngle(p2.getAngle() + value);

			System.out.println("Angle: " + p2.getAngle());
		}
	}
	
	public void changeVelo(int playerNumber, int value)
	{
		if(playerNumber == 1)
		{
			p1.setVelocity(p1.getVelocity() + value);
			
			System.out.println("Velocity: " + p1.getVelocity());
		}
		else
		{
			p2.setVelocity(p2.getVelocity() + value);

			System.out.println("Velocity: " + p2.getVelocity());
		}
	}
}
