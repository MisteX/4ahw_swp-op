import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

public class DrawEngine extends JPanel 
{
	private ThanksModel tm;
	private ThanksContr tc;
	
	public DrawEngine()
	{
		tm = ThanksModel.getInstance();
		tc = ThanksContr.getInstance();
	}

	@Override
	public void paintComponent(Graphics g)
	{
//		System.out.println("\n------------------\n");
		
		int j;
		g.setColor(Color.GREEN);
		
		for(int i = 0; i < tm.getField().length; i++)
		{
			j = ThanksView.getDimensionY() - tm.getField()[i];
			g.fillRect(i*(ThanksView.getDimensionX()/tm.getField().length), ThanksView.getDimensionY() - tm.getField()[i], ThanksView.getDimensionX()/tm.getField().length, tm.getField()[i]);

//			System.out.println("X: " + i*(ThanksView.getDimensionX()/tm.getField().length));
//			System.out.println("Y: " + j);
//			System.out.println("Width: " + ThanksView.getDimensionX()/tm.getField().length);
//			System.out.println("Height: " + tm.getField()[i] + "\n");			
		}
		
		tc.createPlayers();
		g.setColor(Color.RED);
		g.fillRect(tc.getPlayerPositions()[0], ThanksView.getDimensionY() - tm.getField()[tc.getPlayerPositions()[0] / (ThanksView.getDimensionX()/tm.getField().length)] - 10, 10, 10);
		
		g.setColor(Color.BLUE);
		g.fillRect(tc.getPlayerPositions()[1], ThanksView.getDimensionY() - tm.getField()[tc.getPlayerPositions()[1] / (ThanksView.getDimensionX()/tm.getField().length)] - 10, 10, 10);
//		g.fillRect(0, 0, 100, 100);
	}
}
