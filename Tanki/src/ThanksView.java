import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class ThanksView extends JFrame implements ActionListener, KeyListener
{
	private JPanel p = new JPanel();
	DrawEngine de = new DrawEngine();
	private ThanksModel tm;
	private ThanksContr tc = ThanksContr.getInstance();
	private Timer timer = new Timer(25,this);
	
	private static int dimensionX = 1000;
	private static int dimensionY = 700;
	
	private int positionP1;
	private int positionP2; 

	public ThanksView()
	{
		tm = ThanksModel.getInstance();
		
		p.setSize(new Dimension(dimensionX, dimensionY));
		p.setBackground(new Color(50,150,200));
		tc.fillField(50);

//		paintComponent();

		timer.start();
		
		this.setBounds(new Rectangle(new Dimension(dimensionX, dimensionY)));
		this.setLocation(new Point(200,100));
		this.setDefaultCloseOperation(1);
		this.setResizable(false);
//		this.add(p);
		this.add(de);
		
		this.setVisible(true);
	}

	public void posPlayers(int pos1, int pos2)
	{
		
	}
	
	public static int getDimensionX() 
	{
		return dimensionX;
	}
	
	public static int getDimensionY() 
	{
		return dimensionY;
	}

	public int getPositionP1() 
	{
		return positionP1;
	}

	public void setPositionP1(int positionP1) 
	{
		this.positionP1 = positionP1;
	}

	public int getPositionP2() 
	{
		return positionP2;
	}

	public void setPositionP2(int positionP2) 
	{
		this.positionP2 = positionP2;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyPressed(KeyEvent ke) 
	{
		if(ke.getKeyCode()==KeyEvent.VK_UP)
		{
			tc.changeAngle(1, 1);
		}
		if(ke.getKeyCode()==KeyEvent.VK_RIGHT)
		{
			
		}
		if(ke.getKeyCode()==KeyEvent.VK_DOWN)
		{
			tc.changeAngle(1, -1);
		}
		if(ke.getKeyCode()==KeyEvent.VK_LEFT)
		{
			
		}
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
}
