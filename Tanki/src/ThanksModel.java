import java.io.Serializable;

public class ThanksModel implements Serializable
{
	private int[] field = new int[200];
	
//	private Player p1 = new Player();
//	private Player p2 = new Player();

	private static ThanksModel tm = null;
	
	private ThanksModel() { }

	public static ThanksModel getInstance()
	{
		if(tm == null)
		{
			tm = new ThanksModel();
		}
		return tm;
	}
	
	public int[] getField()
	{
		return field;
	}
	public void fillField (int[] height)
	{
		try
		{
			for(int i = 0; i < field.length; i++)
			{
				field[i] = height[i];
				
//				System.out.println(height[i] + " --> " + field[i]);
			}
//			field[] = height[];
		}
		catch (NullPointerException npe)
		{
			System.err.println("Fill with a 50-Elements-Array!");
		}
	}
}
