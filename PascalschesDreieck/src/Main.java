public class Main 
{
	public static void main (String[] args)
	{
		int arg1 = Integer.parseInt(args[0]);
		int arg2 = Integer.parseInt(args[1]);
		System.out.println(pasTri(arg1,arg2));
	}
	
	public static int pasTri(int i, int k)
	{		
		if(k == i || k == 0)
		{
			return 1;
		}
		return pasTri(i-1, k-1) + pasTri(i-1, k);
	}
}
