# Schulrepository Andreas Krapf
Klasse: 4AHW der HTL-Anichstrasse  

---  
## Projekte:
Schiffeversenken
--  
Editoren: Robert Blum / Andreas Krapf  
Sachen zu Machen:  
- Spielarena einf�gen  
- Gameplay implementieren  
  
---     
MD2HTML Converter
--  
- Markdown (md) Dateien sollen als Startbedingung angegeben werden  
- Als Ergebnis soll eine HTML-Datei ausgegeben werden, die man im Internetbrowser �ffnen kann

was ist gemacht:  
* Header 1-6  
* Bolt / Itallic / underlined Schrift  
* Unordered Lists  
* Space-Correction  
* in Datei ausgeben  
* Trennstrich  
---
Sortieralgorithmus
--
- Sortieralgorithmus nach Wahl programmieren  

was ist gemacht:  
* BubbleSort algorithmus  
* Consolenstreamreader  

---
Linked List  
--
- Einen InsertionSort einer Linked List programmieren  

was ist gemacht:  
* InsertionSort einzelner Elemente  
* InsertionSort einer bestehenden Linked List  
* Randomizer zum f�llen

---
Rekursivit�t  
--  
- Stellen des Pascal'schen Dreieck per Rekursivit�t ausrechnen  
- Palindromerkennung �ber Rekursivit�t  
- Endrekursivit�t der Fakult�t  
- Binarysearch Rekursiv  
- Quicksort  
  
was ist gemacht:  
* Pascal'sches Dreieck  
* Palindromerkennung  
* Palindrom: multi-input bei start der Methode  
* Fakult�t: Startwert  
* Fakult�t: Normale Rekursivit�t  
* binarysearch  
* Quicksort-Halbcheat  

---  
Pseudo-Code
--  
- gegeben ist ein Pseudocode und daraus musste ein Programm geschrieben werden  
- Pseudocode: Insertionsort  
  
was ist gemacht:  
* Insertionsort �ber den Pseudocode auf Java geschrieben  
  
---
CanBan-System
--  
- gegeben waren Storycards, die die Systemanforderungen enthielten  
- Angabe:  
* Software zur Organisation von Auftr�gen. Die Task sollen einzeln auf Storycards gespeichert werden  
* Storycards anlegen  
* Storycards auf Stapel legen (ToDo, Doing, Done)  
* Storycards zwischen den Stapeln wechseln  
* Daten abspeichern und neu aufrufen  
* Zeitsch�tzung und gebrauchte Zeit
  
was ist gemacht:  
* Software  
* Storycards anlegen  
* Zwischen Stapel wechseln  
* Auf Stapel legen  
* Speicherung via MySQL-Datenbank  
  
---
MVC-Uebung  
--  
- gegeben war eine Aufgabe als MVC-Konstruktion zu loesen  
- Ein Schiff mit Besatzung und Speicherung  
  
was ist gemacht:  
* Lauffaehige Software  
* Speicherung  
* wieder aufrufbar  

---
Tanki
--  
- das Spiel Tanki programmieren  
- Spielfeld per GUI  
- Schiefer Wurf f�r Schuss  
- Winkel und Gesschwindigkeitseinstellung  

was ist gemacht:  
* Spielfeld mit Spielersetzung  
* Anfang Winkeleinstellung  