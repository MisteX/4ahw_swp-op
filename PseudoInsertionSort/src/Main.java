/*
 * INSERTION SORT(A)
 * FOR i = 2 TO n
 * 		h = A[i]
 * 		j = i - 1
 * 		WHILE A[j] > h AND j >= 0 DO
 * 			A[j+1] = A[j]
 * 			j = j - 1
 * 		A[j+1] = h
 */

import java.util.ArrayList;

public class Main 
{
	public static void main(String[] args)
	{
		ArrayList<Integer> al = new ArrayList<Integer>();

		al.add(7);
		al.add(4);
		al.add(6);
		al.add(10);
		al.add(9);
		al.add(3);
		
		al = insertionSort(al);
		
		for(int i=0; i< al.size(); i++)
		{
			System.out.println(al.get(i));
		}
	}
	
	public static ArrayList<Integer> insertionSort(ArrayList<Integer> al)
	{
		int h;
		int j;
		
		for(int i = 1; i < al.size(); i++)
		{
			h = al.get(i);
			j = i - 1;
			
			while(j >= 0 && al.get(j) > h)
			{
				al.set(j + 1, al.get(j));
				j = j - 1;
			}
			al.set(j + 1, h);
		}
		return al;
	}
}
