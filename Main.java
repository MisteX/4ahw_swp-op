import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {

	public static ArrayList<Integer> randomNumbers = new ArrayList<Integer>();

	public static void fillArray (double maxNumber, int numberofInserts)
	{

		for(int i = 0; i< numberofInserts; i++)
		{
			randomNumbers.add((int)(Math.random()* maxNumber));
		}
		
		System.out.println("Completed filling!");
		System.out.println("Your Array is:");
		
		for(int i = 0; i< randomNumbers.size();i++)
		{
			System.out.print(randomNumbers.get(i)+ " | ");
		}
	}

	public static void sortArray()
	{
		int numberOne;
		int numberTwo;
		int c = 0;

		while (c < randomNumbers.size()*randomNumbers.size()) 
		{
			numberOne = randomNumbers.get(c%randomNumbers.size());
			numberTwo = randomNumbers.get((c + 1)%randomNumbers.size());

			if (numberOne > numberTwo && (c + 1)%randomNumbers.size() != 0) 
			{
				randomNumbers.set(c%randomNumbers.size(), numberTwo);
				randomNumbers.set((c + 1)%randomNumbers.size(), numberOne);
			}
			c++;
		} 
		System.out.println("\nYour Array is now:");
		for(int i = 0; i< randomNumbers.size();i++)
		{
			System.out.print(randomNumbers.get(i)+ " | ");
		}
	}

	public static void main(String[] args) 
	{		
		int maxN;
		int maxV;
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);

		try 
		{
			System.out.print("Do you want to fill an Array? [y/n] ");

			if(br.readLine().equals("y"))
			{
				System.out.println("How many elements do you want?");
				maxN = Integer.parseInt(br.readLine());
				
				System.out.println("Whats the maximum value you want?");
				maxV = Integer.parseInt(br.readLine());

				fillArray((double) maxV, maxN);

				System.out.println("\n Now that you filled an Array, do you want to sort it? [y/n]");
				if(br.readLine().equals("y"))
				{
					sortArray();
					System.out.println("\nThank you for working with us ;D");
				}
				else
				{
					System.out.println("Why have you filled it then? �.o");
				}
			}
			else
			{
				System.out.println("Why are you starting this program? �.o");
			}
		} 
		catch (NumberFormatException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
}