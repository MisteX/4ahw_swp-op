import java.io.Serializable;

public class Mensch implements Serializable
{
	private String vorname;
	private String nachname;
	private String rang;
	
	public Mensch () { }
	public Mensch (String vorname, String nachname, String rang)
	{
		setNachname(nachname);
		setVorname(vorname);
		setRang(rang);
	}

	public String getVorname() 
	{
		return vorname;
	}

	public void setVorname(String vorname) 
	{
		this.vorname = vorname;
	}

	public String getNachname() 
	{
		return nachname;
	}

	public void setNachname(String nachname) 
	{
		this.nachname = nachname;
	}

	public String getRang() 
	{
		return rang;
	}

	public void setRang(String rang) 
	{
		this.rang = rang;
	}
}
