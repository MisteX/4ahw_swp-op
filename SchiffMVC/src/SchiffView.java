import java.util.ArrayList;

public class SchiffView 
{
	private SchiffController sc = SchiffController.getInstance();
	private ArrayList<SchiffModel> schiffe = new ArrayList<SchiffModel>();
	
	public SchiffView ()
	{
		sc.insertSchiffe();
		schiffe = sc.getSchiffe();
		
		for(int i = 0; i < schiffe.size(); i++)
		{
			System.out.println(schiffe.get(i).getName());
			System.out.println("Laenge: " + schiffe.get(i).getLaenge() + " | Breite: " + schiffe.get(i).getBreite());
			System.out.println("Kapitaen: " + schiffe.get(i).getKapitaen().getNachname() + " " + schiffe.get(i).getKapitaen().getVorname());
			System.out.println("Besatzung: ");
			for(int j = 0; j < schiffe.get(i).getBesatzungsnamen().size(); j++)
			{
				System.out.println(schiffe.get(i).getBesatzungsnamen().get(j));
			}
			
			System.out.println("\n\n");
		}
	}
}
