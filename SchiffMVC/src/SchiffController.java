import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class SchiffController 
{
	private static SchiffController Instance = null;

	ArrayList<SchiffModel> schiffe = new ArrayList<SchiffModel>();

	private SchiffController () { }

	public static SchiffController getInstance()
	{
		if(Instance == null)
		{
			Instance = new SchiffController();
		}
		return Instance;
	}

	public void insertSchiffe()
	{
		try
		{
			schiffe = loadShip();
//			System.out.println(sm.getBreite());
//			System.out.println(sm.getLaenge());
//			System.out.println(sm.getName());
//			System.out.println(sm.getKapitaen());
//			schiffe.add(sm);
		}
		catch (Exception ex)
		{
//			ex.printStackTrace();
			
			Mensch ma1 = new Mensch("Kurt","Schurbert","Oberoffizier");
			Mensch ma2 = new Mensch("Hannes","Waldrei","Leutnant");
			Mensch ma3 = new Mensch("Werner","Harad","Leutnant");
			Mensch ma4 = new Mensch("Dogu","Kritsotaki","Oberoffizier");
			Mensch ma5 = new Mensch("Max","Mustermann","Matrose");
			Mensch ma6 = new Mensch("Ralph","Carriker","Matrose");
			Mensch ma7 = new Mensch("Adolf","Krummetsberger","Matrose");
			Mensch ma8 = new Mensch("Patrick","Neuhauser","Matrose");
			Mensch ma9 = new Mensch("Tobias","Bernhard","Matrose");
			Mensch ma10 = new Mensch("Jan","Posti","Matrose");
			Mensch ma11 = new Mensch("Paul","Nigg","Matrose");
			Mensch ma12 = new Mensch("Albert","Krael","Matrose");
			Mensch ma13 = new Mensch("Manuel","Hopinger","Matrose");
			Mensch ma14 = new Mensch("Deckard","Pawlon","Matrose");
			Mensch ma15 = new Mensch("Dominik","Hantinger","Leutnant");
			Mensch ma16 = new Mensch("Benjamin","Krupp","Leutnant");
			Mensch ma17 = new Mensch("Elias","Eritor","Unteroffizier");
			Mensch ma18 = new Mensch("Shaun","Poltery","Unteroffizier");

			ArrayList<Mensch> bes1 = new ArrayList<Mensch>();
			ArrayList<Mensch> bes2 = new ArrayList<Mensch>();
			ArrayList<Mensch> bes3 = new ArrayList<Mensch>();
			ArrayList<Mensch> bes4 = new ArrayList<Mensch>();

			//bes1.add(ma18);
			bes1.add(ma2);
			bes1.add(ma8);
			bes1.add(ma12);
			bes1.add(ma14);

			//bes2.add(ma1);
			bes2.add(ma16);
			bes2.add(ma6);

			//bes3.add(ma17);
			bes3.add(ma3);
			bes3.add(ma5);
			bes3.add(ma7);
			bes3.add(ma9);
			bes3.add(ma10);
			bes3.add(ma11);

			//bes4.add(ma4);
			bes4.add(ma15);
			bes4.add(ma13);
			
			SchiffModel schiff1 = new SchiffModel(bes1, 49.4, 18.1, "Eureka", ma18);
			SchiffModel schiff2 = new SchiffModel(bes2, 29.0, 5.9, "Bury", ma1);
			SchiffModel schiff3 = new SchiffModel(bes3, 70.8, 24.0, "Rapier", ma17);
			SchiffModel schiff4 = new SchiffModel(bes4, 38.2, 7.5, "Z34", ma4);

			schiffe.add(schiff1);
			schiffe.add(schiff2);
			schiffe.add(schiff3);
			schiffe.add(schiff4);
		}
		
		try
		{
			saveShip();
		}
		catch(IOException ex)
		{
			System.err.println("Could not save the file!");
		}
	}

	public ArrayList<SchiffModel> getSchiffe() 
	{
		return schiffe;
	}

	public ArrayList<SchiffModel> loadShip() throws IOException
	{
		ArrayList<SchiffModel> sms = new ArrayList<SchiffModel>();
		
		FileInputStream fis = new FileInputStream("shipModel.xml");
		XMLDecoder decoder = new XMLDecoder(fis);
		
		sms = SchiffModel.deserializeFromXML(decoder);
		
		decoder.close();
		fis.close();
		
		return sms;
	}
	
	public void saveShip() throws IOException
	{
		FileOutputStream fos = new FileOutputStream("shipModel.xml");
		XMLEncoder enc = new XMLEncoder(new BufferedOutputStream(fos));
		
		for(int i = 0; i < schiffe.size(); i++)
		{
			SchiffModel.serializeToXML(schiffe.get(i), schiffe.get(i).getBesatzung(), enc);
		}
		
		enc.close();
		fos.close();
	}
}