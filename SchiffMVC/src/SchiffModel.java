import java.beans.ExceptionListener;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

public class SchiffModel implements Serializable
{
	private ArrayList<Mensch> besatzung;
	private double laenge;
	private double breite;
	private String name;
	private Mensch kapitaen;

	public SchiffModel() { }
	public SchiffModel (double laenge, double breite, String name)
	{
		setLaenge(laenge);
		setBreite(breite);
		setName(name);
	}
	public SchiffModel (ArrayList<Mensch> besatzung, double laenge, double breite, String name, Mensch kapitaen)
	{
		setLaenge(laenge);
		setBreite(breite);
		setName(name);
		setBesatzung(besatzung);
		setKapitaen(kapitaen);
	}

	public ArrayList<String> getBesatzungsnamen() 
	{
		ArrayList<String> namen = new ArrayList<String>();

		for(int i = 0; i < besatzung.size(); i++)
		{
			if(besatzung.size() == 0)
			{
				System.out.println("No Crew mentioned!");
			}
			else
			{
				namen.add(besatzung.get(i).getNachname() + " " + besatzung.get(i).getVorname());
			}
		}
		return namen;
	}

	public void setBesatzung(ArrayList<Mensch> besatzung) 
	{
		this.besatzung = besatzung;
	}
	public ArrayList<Mensch> getBesatzung() 
	{
		return besatzung;
	}

	public double getLaenge() 
	{
		return laenge;
	}

	public void setLaenge(double laenge) 
	{
		this.laenge = laenge;
	}

	public double getBreite() 
	{
		return breite;
	}

	public void setBreite(double breite) 
	{
		this.breite = breite;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}

	public Mensch getKapitaen() 
	{
		return kapitaen;
	}

	public void setKapitaen(Mensch kapitaen) 
	{
		this.kapitaen = kapitaen;
	}

	public static void serializeToXML (SchiffModel sm, ArrayList<Mensch> menschen, XMLEncoder enc) throws IOException
	{
//		FileOutputStream fos = new FileOutputStream("shipModel.xml");
//		XMLEncoder encoder = new XMLEncoder(fos);
//		XMLEncoder enc = new XMLEncoder(new BufferedOutputStream(fos));
//		encoder.setExceptionListener(new ExceptionListener() {
//			public void exceptionThrown(Exception e) {
//				System.out.println("Exception! :"+e.toString());
//			}
//		});
		
		enc.writeObject(sm);
		enc.writeObject(menschen);
		
//		enc.close();
//		fos.close();
	}

	public static ArrayList<SchiffModel> deserializeFromXML(XMLDecoder decoder) throws IOException {
//		FileInputStream fis = new FileInputStream("shipModel.xml");
//		XMLDecoder decoder = new XMLDecoder(fis);
		ArrayList<SchiffModel> decodedShips = (ArrayList<SchiffModel>) decoder.readObject();
//		decoder.close();
//		fis.close();
		return decodedShips;
	}
}
