public class Main 
{
	public static void main (String[] args)
	{
		MyLinkedList mylinkedlist = new MyLinkedList();
		Randomizer random = new Randomizer();

		//mylinkedlist.insertSorted(5);
		//mylinkedlist.insertSorted(9);
		//mylinkedlist.insertSorted(2);
		//mylinkedlist.insertSorted(10);
		//mylinkedlist.insertSorted(7);
		//mylinkedlist.insertSorted(69);
		//mylinkedlist.insertSorted(22);
		//mylinkedlist.insertSorted(99);

		//mylinkedlist.add(5);
		//mylinkedlist.add(9);
		//mylinkedlist.add(2);
		//mylinkedlist.add(10);
		//mylinkedlist.add(7);
		//mylinkedlist.add(2);
		//mylinkedlist.add(22);
		//mylinkedlist.add(7);
		//mylinkedlist.add(7);
		//mylinkedlist.add(7);
		//mylinkedlist.add(2);
		//mylinkedlist.add(5);
		//mylinkedlist.add(4);
		//mylinkedlist.add(7);
		//mylinkedlist.add(9);
		//mylinkedlist.add(13);
		
		mylinkedlist.add(random.getIntFromZero(10));
		mylinkedlist.add(random.getIntFromZero(10));
		mylinkedlist.add(random.getIntFromZero(10));
		mylinkedlist.add(random.getIntFromZero(10));
		mylinkedlist.add(random.getIntFromZero(10));
		mylinkedlist.add(random.getIntFromZero(10));
		mylinkedlist.add(random.getIntFromZero(10));
		mylinkedlist.add(random.getIntFromZero(10));
		mylinkedlist.add(random.getIntFromZero(10));
		mylinkedlist.add(random.getIntFromZero(10));
		mylinkedlist.add(random.getIntFromZero(10));
		mylinkedlist.add(random.getIntFromZero(10));
		mylinkedlist.add(random.getIntFromZero(10));

		Node node = mylinkedlist.getHead();
		do 
		{
			System.out.println(node.getValue());
			node = node.getNext();
		} 
		while (node.hasNext());

		System.out.println(node.getValue());
		System.out.println("=======" + mylinkedlist.getSize() + "=======");	

		mylinkedlist = mylinkedlist.insertionSort(mylinkedlist);

		node = mylinkedlist.getHead();
		do 
		{
			System.out.println(node.getValue());
			node = node.getNext();
		} 
		while (node.hasNext());

		System.out.println(node.getValue());
		System.out.println("=======" + mylinkedlist.getSize() + "=======");
	}
}
