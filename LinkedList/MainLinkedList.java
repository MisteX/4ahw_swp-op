import java.util.ArrayList;

public class Main {

	public static void main(String[] args) 
	{

		MyLinkedList mll = new MyLinkedList();
		mll.add(5);
		mll.add(10);
		mll.add(12);
		
		Node node = mll.getHead();
		do 
		{
			System.out.println(node.getValue());
			node = node.getNext();
		} 
		while (node.hasNext());
		
		System.out.println(node.getValue());
		System.out.println("=======" + mll.getSize() + "=======");
		
		
		mll.delete(1);
		
		for (int i = 0; i < mll.getSize(); i++) 
		{
			System.out.println(mll.getValue(i));
		}
		
		System.out.println("=======" + mll.getSize() + "=======");
		
	}

}
