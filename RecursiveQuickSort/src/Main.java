import java.util.ArrayList;
import java.util.Collections;

public class Main 
{
	public static void main (String[] args)
	{
		ArrayList<Integer> al = new ArrayList<Integer>();

		al.add(7);
		al.add(4);
		al.add(10);
		al.add(19);
		al.add(3);
		al.add(1);
		al.add(5);
		al.add(8);
		al.add(4);
		al.add(2);
		al.add(99);
		al.add(13);
		al.add(31);
		al.add(50);
		al.add(1000000);
		
		for(int i = 0; i < al.size();i++)
		{
			System.out.println(al.get(i));
		}
		
		System.out.println("====");
		quicksort(al);
		
		for(int i = 0; i < al.size();i++)
		{
			System.out.println(al.get(i));
		}
	}

	public static ArrayList<Integer> quicksort (ArrayList<Integer> al)
	{
		sorting(al, 0, al.size() - 1);
		return al;
	}

	private static void sorting (ArrayList<Integer> al, int start, int end)
	{

		if(end - start <= 1) 
		{
			return;
		}

		int lmnt;
		int pivot = al.get(start);
		int counter = start;
		int help;

		for(int i = start + 1; i <= end; i++)
		{
			lmnt = al.get(i);
			if(lmnt < pivot)
			{
				al.add(counter, lmnt);
				al.remove(i+1);
				counter++;
			}
		}
		sorting(al, counter + 1, end);
		sorting(al, 0, counter);
	}
}
