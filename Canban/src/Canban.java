import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.JScrollBar;

public class Canban {

	private JFrame frmCanbansystem;
	private DBManager db;
	private Listung listung;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Canban window = new Canban();
					window.frmCanbansystem.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Canban() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCanbansystem = new JFrame();
		frmCanbansystem.setTitle("Canban-System");
		frmCanbansystem.setBounds(100, 100, 750, 439);
		frmCanbansystem.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmCanbansystem.getContentPane().setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 728, 383);
		frmCanbansystem.getContentPane().add(panel);
		panel.setLayout(null);

		JTextArea textArea = new JTextArea();
		textArea.setBounds(15, 161, 655, 160);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);

		JScrollPane scrollPane = new JScrollPane(textArea);
		scrollPane.setBounds(15, 161, 683, 160);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);

		panel.add(scrollPane);

		JButton btnNewButton = new JButton("Hinzuf\u00FCgen");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				if(!textField.getText().equals("")|| !textArea.getText().equals(""))
				{
					db.addStorycard(textField.getText(), textArea.getText());
					textField.setText("");
					textArea.setText("");
				}
			}
		});
		btnNewButton.setBounds(15, 337, 115, 29);
		panel.add(btnNewButton);

		JButton btnAuflisten = new JButton("Auflisten");
		btnAuflisten.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					listung = new Listung();
					listung.setVisible(true);
					listung.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				} catch (SQLException e) {
					e.printStackTrace();
				}

			}
		});
		btnAuflisten.setBounds(598, 337, 115, 29);
		panel.add(btnAuflisten);

		textField = new JTextField();
		textField.setBounds(15, 50, 683, 41);
		panel.add(textField);
		textField.setColumns(10);

		JLabel lblStorycardname = new JLabel("Storycard-Name:");
		lblStorycardname.setBounds(15, 16, 151, 20);
		panel.add(lblStorycardname);

		JLabel lblStorycardbeschreibung = new JLabel("Storycard-Beschreibung:");
		lblStorycardbeschreibung.setBounds(15, 125, 216, 20);
		panel.add(lblStorycardbeschreibung);

		try {
			db = new DBManager();
			db.create();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
}
