import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Read {
	private ArrayList<String> text;

	public Read() {

	}

	public ArrayList<String> readIn() throws IOException {
		ArrayList<String> lines = new ArrayList<String>();
		FileReader fr = new FileReader(System.getProperty("user.dir") + "\\CREATE.txt");
		BufferedReader br = new BufferedReader(fr);
		String line = "";
		try {
			// fr = new FileReader(System.getProperty("user.dir") + "\\CREATE.txt");
			// br = new BufferedReader(fr);
			line = br.readLine();
			lines.add(line);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return lines;
	}

	public ArrayList<String> getText() {
		return text;
	}

	public void setText(ArrayList<String> text) {
		this.text = text;
	}
}