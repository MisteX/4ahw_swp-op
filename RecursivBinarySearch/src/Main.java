import java.util.ArrayList;

public class Main {

	public static void main(String[] args) 
	{

		ArrayList<Integer> values = new ArrayList<Integer>();
		for(int i=0; i<=10; i++)
		{
			values.add(i);
		}

		values.add(12);
		
		System.out.println("Die gesuchte Zahl liegt auf Position: "+binarySearch(12,values));
		System.out.println("Die gesuchte Zahl liegt auf Position: "+binarySearch(3,values));
		System.out.println("Die gesuchte Zahl liegt auf Position: "+binarySearch(5,values));
		System.out.println("Die gesuchte Zahl liegt auf Position: "+binarySearch(10,values));
		System.out.println("Die gesuchte Zahl liegt auf Position: "+binarySearch(6,values));
		System.out.println("Die gesuchte Zahl liegt auf Position: "+binarySearch(9,values));
		System.out.println("Die gesuchte Zahl liegt auf Position: "+binarySearch(11,values));
		System.out.println("Die gesuchte Zahl liegt auf Position: "+binarySearch(0,values));
	}

	public static int binarySearch (Integer val, ArrayList<Integer> al)
	{
		return binSearch(val,al,0,al.size());		
	}

	private static int binSearch (Integer val, ArrayList<Integer> al, int start, int end)
	{
		int index = (start + end) / 2;

		if (val > al.get(al.size()-1) || val < al.get(0))
		{
			return -1;
		}
		if(al.get(index) < val && al.get(index+1) > val)
		{
			return -1;
		}
		if (al.get(index) > val) 
		{
			return binSearch(val, al, start, index);
		}
		if (al.get(index) < val) 
		{
			return binSearch(val, al, index, end);
		}
		return index;
	}
}
