
public class Main {

	public static void main(String[] args) 
	{
//		System.out.println(fact(5));	
		System.out.println("Normale Rekursivitšt: " + factRec(Integer.parseInt(args[0])));	
		System.out.println("Endrekursivitšt: " + factEndRec(Integer.parseInt(args[0])));
	}
	
	public static int fact (int n)
	{
		
		int ret = n;
		
		for(int i = 1; i < n; i++)
		{
			ret = ret * (n-i);
			System.out.println(ret);
		}
		
		return ret;
	}
	
	public static int factRec(int n)
	{
		if(n == 2)
		{
			return 2;
		}
		if(n == 1 || n == 0)
		{
			return 1;
		}
		if(n < 0)
		{
			return 0;
		}
		
		return n * factRec(n-1);
	}
	
	public static int factEndRecAllg (int n, int m)
	{
		if (n==2)
		{
			return 2*m;
		}
		
		return factEndRecAllg(n-1, m * n);
	}
	
	public static int factEndRec(int n)
	{
		return factEndRecAllg(n, 1);
	}

}
