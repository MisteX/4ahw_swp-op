public class Randomizer 
{
	public Randomizer() { }
	
	public int getInt(int lowerBorder, int upperBorder)
	{
		int number = (int) (Math.random() * upperBorder);
		
		while (number < lowerBorder)
		{
			number = (int) (Math.random() * upperBorder);
		}
		
		return number;
	}
	
	public int getIntFromZero(int upperBorder)
	{
		int number = (int) (Math.random() * upperBorder);
		
		return number;
	}
	
	public double getDouble(double lowerBorder, double upperBorder)
	{
		double number = Math.random() * upperBorder;
		
		while (number < lowerBorder)
		{
			number = Math.random() * upperBorder;
		}
		
		return number;
	}
	
	public double getDoubleFromZero(double upperBorder)
	{
		double number = Math.random() * upperBorder;
		
		return number;
	}
}
